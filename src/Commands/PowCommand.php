<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class PowCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'pow {base} {exp}'; 

    /**
     * @var string
     */
    protected $description = 'exponent all given Numbers';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $base   = $this->argument('base');
        $exp    = $this->argument('exp');
        $pow    = pow($base,$exp);

        $this->comment($base .' ^ '. $exp . ' = ' . $pow ."\n");
    }
}
